if test (fish --version | string match -ar '\d' | string join '') -lt 300
  echo 'This plugin is compatible with fish version 3.0.0 or above, please update before trying to use it' 1>&2
else if type -q omf
  omf update contains_opts >/dev/null 2>&1
  or omf install https://gitlab.com/lusiadas/contains_opts
else
  fisher add gitlab.com/lusiadas/contains_opts >/dev/null
end